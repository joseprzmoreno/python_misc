import copy

sentence_pairs = [
    ["the book","das Buch"],
    ["the house","das Haus"],
    ["a book","ein Buch"]
]

#function to print translation probabilites
def print_translation_probabilities(tp):
    print("---------------------------------")
    for key,value in tp.items():
        print(key)
        for key_,value_ in value.items():
            print("\t",key_,value_)
    print("//////////////////////////////////")

#function to compare two two-dimensional dictionaries
def check_same_translation_probabilities(tp1,tp2):
    are_same = True
    for key,value in tp1.items():
        go_out = False
        for key_,value_ in value.items():
            if tp1[key][key_] != tp2[key][key_]:
                go_out = True
                break
        if go_out:
            are_same = False
            break
    return are_same

#get sentences separated by language
sentences_eng = []
sentences_ger = []
for sp in sentence_pairs:
    sentences_eng.append(sp[0])
    sentences_ger.append(sp[1])

#get individual words
words_eng = []
words_ger = []
for s in sentences_eng:
    words = s.split(" ")
    for w in words:
        words_eng.append(w)
for s in sentences_ger:
    words = s.split(" ")
    for w in words:
        words_ger.append(w)
words_eng = sorted(list(set(words_eng)))
words_ger = sorted(list(set(words_ger)))

#prepare storage for translation probabilites. Initialize uniformly
translation_probabilities = {}
for word_eng in words_eng:
    translation_probabilities[word_eng] = {}
    for word_ger in words_ger:
        prob = round(1/len(words_ger),2)
        translation_probabilities[word_eng][word_ger] = prob
print_translation_probabilities(translation_probabilities)

iterations = 0
converged = False
while not converged:
    iterations+=1
    print("iteration #{}".format(iterations))
    former_tp = copy.deepcopy(translation_probabilities)
    count = {}
    for word_eng in words_eng:
        count[word_eng] = {}
        for word_ger in words_ger:
            count[word_eng][word_ger] = 0
    total = {}
    for word_ger in words_ger:
        total[word_ger] = 0
    for sp in sentence_pairs:
        #compute normalization
        words_eng_in_s = sp[0].split(" ")
        words_ger_in_s = sp[1].split(" ")
        s_total = {}
        for word_eng in words_eng_in_s:
            s_total[word_eng] = 0
            for word_ger in words_ger_in_s:
                s_total[word_eng] += translation_probabilities[word_eng][word_ger]
        #collect counts
        for word_eng in words_eng_in_s:
            for word_ger in words_ger_in_s:
                count[word_eng][word_ger] += round(translation_probabilities[word_eng][word_ger] / s_total[word_eng],2)
                total[word_ger] += round(translation_probabilities[word_eng][word_ger] / s_total[word_eng],2)
    #estimate probabilities
    for word_ger in words_ger:
        for word_eng in words_eng:
            translation_probabilities[word_eng][word_ger] = round(count[word_eng][word_ger] / total[word_ger],2)
    print_translation_probabilities(translation_probabilities)
    #check converged
    same_tp = check_same_translation_probabilities(former_tp, translation_probabilities)
    if same_tp:
        converged = True

            
    
